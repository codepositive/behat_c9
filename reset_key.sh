#!/bin/bash
cat << END > ~/workspace/KEY
====  TRAINING KEY ====
The last line of this file should contain 
a single number from 1 to 9. This number
is your training key. No other person on
the course should be using the same number.


== KEY ==
x
END