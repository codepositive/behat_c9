#!/bin/bash
SSH_KEYFILE=~/workspace/training.rsa
SERVER=training@plus.codepositive.com



find ~/workspace/KEY -mmin +420 -exec reset_key.sh \;  
USERNUMBER=`tail -1 ~/workspace/KEY`

function usage() {
    echo "Please set the value in KEY to your training user number"
    c9 ~/workspace/KEY
    exit
}

function no_ssh_key(){
    echo "The is no SSH identity file available. You need to upload the training.rsa file to the main workspace directory."
    exit;
}

[[ $USERNUMBER =~ ^-?[0-9]$ ]] || usage;

echo "clearing any old tunnels..."
kill $(ps ux | grep 444[4] | awk '{print $2}')
echo "Creating tunnel to Code Positive server";
ssh -L 4444:localhost:4444${USERNUMBER} -R 880${USERNUMBER}:localhost:8080 -f -N




