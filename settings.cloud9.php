<?php

$databases['default']['default'] = array (
  'database' => 'c9',
  'username' => 'root',
  'password' => '',
  'prefix' => '',
  'host' => '0.0.0.0',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$settings['install_profile'] = 'standard';
$config_directories['sync'] = 'sites/default/sync';
