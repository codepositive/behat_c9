Code Positive BEHAT Training Cloud 9 setup
===========================================
Welcome to Code Positive's BEHAT training environment. 

There are just a few steps you need to follow to get up and running. 
This document will outline what needs to be done, but you can also get step by 
using the preview. 


# Create a cloud 9 server
Step 1 is to create a virtual machine on Cloud9. 

* Create and log into an account on http://c9.io
* Click on "Create a new workspace"
* Give your workspace a name
* You can make your workspace public or private (it does not matter)
* Enter this git url as the source : https://bitbucket.org/codepositive/behat_c9.git/behat_c9.git
* Select a PHP, Apache & Mysql project
* Hit create
* Sit back and watch your new virtual machine get started



## Cloud 9 Installation Step by step 
Once your cloud 9 environment is up and running, follow these steps to get a working test environment.

### Get some assistance
* Click "Run Project" in the top bar
* Click Preview -> Preview Running Aplication to open the step by step guide
* Follow the steps in the preview window (or the rest of this file)


### Install Drupal 
* In a Cloud 9 Terminal window run '/home/ubuntu/workspace/install.sh'.
  This may take up to 20 minutes before showing any output, after that the terminal will display 
  messages as it downloads and installs components. The whole process may take over an hour.

### Connect 
* Edit key file in Cloud 9, found in left column, to have a unique number. 1-9 (The Key number you should have received by email)
* In Cloud 9 run command: ./connect.sh (You will be asked for a password)
 
### Local setup
In order to control your local browser you need to have Selenium running on your local computer, and a connection from your machine to Cloud9. 

#### Windows
* Download an easy setup for Windows here: https://bitbucket.org/codepositive/behat_c9/downloads/behat_client.zip
* Extract the zip
* Click connect.bat on your own machine (Get this from a USB or download it from here. Enter the password again. )

#### Mac OS X
* Make sure you have Chrome browser installed at the top level of your Applications directory
* Download an easy setup for Mac here: https://bitbucket.org/codepositive/behat_c9/downloads/behat_client-mac.zip
* Extract the zip
* In your Terminal go to the easy setup folder (Type in 'cd ' and then drag and drop the folder into your Terminal and press Return).
* In your Terminal run './connect_mac.sh'

## Details
### Drupal site 
The install script will automatically set up a new site with these details.

* path: /drupal/web
* Username: admin
* Password: password