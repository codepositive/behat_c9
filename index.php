<html>
<head>
<style type="text/css">
 body {
    font-family: sans-serif;
    }
</style>    
</head>    
<body>
<?php

// Report simple running errors
error_reporting(E_ERROR | E_PARSE);

function get_c9_tunnel_key(){
    $tunnel_key = `ps ux | grep 444[4] | cut -d : -f 5 | cut -c 5`;
    return $tunnel_key;
}


function test_c9_tunnel(){
    $tunnel_key = get_c9_tunnel_key();
    return $tunnel_key;
}


// test for an active selenium connection
function test_selenium(){
    $status = false;
    if ($selenium  = file_get_contents('http://localhost:4444/'))  {
      $pos = stripos($selenium, 'Selenium');
      if ($pos !== false) {
          $status = true;
      }
    }
    return $status;
}

function test_key(){
    $key = file_get_contents('/home/ubuntu/workspace/KEY');
    $exploded = explode( "\n",$key);
    array_pop($exploded);
    $key = array_pop($exploded);
    $status = ((integer) $key > 0 ) and ((integer) $key < 10);
    return $status;
}




function test_drupal(){
    $output = shell_exec('/home/ubuntu/workspace/drupal/vendor/bin/drush @behat st');
    $result = preg_match ( "/Drupal bootstrap\s*:\s*Successful/", $output );
    return $result;
}


function message_working(){
    $message = "<div class='message success'><h2>We're all connected</h2><p>There is now a working connection with your local Selenium server. You should all be set, just make sure that Chrome is installed, if you're using a Mac check that Chrome is at the top level of the Applications folder.</p>
    <p>The preview browser does not work very well, so click the icon to 'Pop out into new window'.</p>
    <p>Your Drupal website is available at two addresses:
    <ul>
    <li><a href='/drupal/web'>Online</a></li>
    <li><a href='http://127.0.0.1:8080/drupal/web'>Local tunnel</a></li>
    </ul>
    </div>";
    return $message;
}


function message_drupal(){
    $message = "<div class='message drupal'><h2>Now install Drupal</h2>
      <p>In order to test something, you need to have a working copy of Drupal installed. </p>
      <p>
      To get Drupal and Behat installed:
      <ol>
      <li>Open a new terminal window in C9 (alt-T) and run '/home/ubuntu/workspace/install.sh'. </li>
      <li>Wait</li>
      </ol>
      </p>
      ";
    return $message;
}


function message_key(){
    $message = "<div class='message key'><h2>Set your KEY</h2><p>In order to connect to selenium you need to set a unique value from 1 to 9 in the KEY file. Open '~/workspace/KEY' in your editor and replace the 'x' with your training number. </p></div>";
    return $message;
}


function message_connect_c9(){
    $message = "<div class='message connect'><h2>Now set up a tunnel</h2>
      <p>To connect your local computer to your BEHAT testing cloud 9 site, we need to create a secret tunnel!. </p>
      <p>
      Establish your tunnel with these steps:
      <ol>
      <li>Open the KEY file in C9 and add your training number to it. 
      <li>Open a new terminal window in C9 (alt-T) and run '~/workspace/connect.sh'. Enter the password when prompted. </li>
      </ol>
      </p>
      ";
    return $message;
}


function message_connect(){
    $message = "<div class='message connect'><h2>We're not connected to your computer</h2>
      <p>You need to be running selenium and a remote connection from your local computer for behat to work.</p>
      <p>
      Make sure that you have performed the following steps:
      <ol>
      <li>If you don't have the local files you need, download and extract the <a href='https://bitbucket.org/codepositive/behat_c9/downloads/behat_client.zip'>behat_client</a>.</li>
      <li>Run (double-click) 'connect.bat'.   When prompted enter the your training number and the password.</li>
      </ol>
      </p>
      <p><em>Make sure that you don't close the windows that pop up. If you have already performed these steps and need to start again, make sure that you close down any previous windows before trying again</em>. </p>
      ";
    return $message;
}


function message_next(){
    $message = "<div class='navigation next'><a href='?".rand(0,1000)."'>TEST AND ADVANCE TO NEXT STEP IF SUCESSFULL</a> </div>";
    return $message;
}

$selenium_status = test_selenium();

if (!test_drupal()) {
  print message_drupal();
  print message_next();
} else {
  if (!$selenium_status) {
      //  we don't have a working selenium connection
      if (!test_key()) {
          // we have not set the key
          print message_key();
      } 
      else {
          //we have a key
          if (get_c9_tunnel_key()) {
            // we have a c9 connection
            print message_connect();    
          }
          else {
              print message_connect_c9();
          }
          
      }
      print message_next();
  } else {
     print message_working();
  }  
}

?>
</body>
</html>

