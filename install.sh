#!/bin/bash
#SET PATH
cat ~/workspace/.profile >> ~/.profile;
. ~/workspace/.profile 

#INSTALL DRUPAL
composer create-project drupal-composer/drupal-project:8.x-dev drupal --stability dev --no-interaction
#ADD BEHAT 
cd ~/workspace/drupal; composer require drupal/drupal-extension:~3.0 guzzlehttp/guzzle:^6.0@dev

# INSTALL SITE
cd ~/workspace/drupal/web/sites/default
drush si standard -y --account-pass=password --db-url=mysql://root@0.0.0.0/c9 --site-name=BEHAT

# INSTALL DRUSH ALIAS
mv  ~/workspace/aliases.drushrc.php ~/.drush

# Initialise behat 
cd ~/workspace; 
behat --init

#edit key
./reset_key.sh
c9 KEY
c9 tunnel.png

export $PATH
